#!/usr/bin/python3

import numpy as np
from random import randint
import copy

def sigmanoid(x):
    return 1/(1+np.exp(-x))

def deriv_sigmanoid(out):
    return out * (1-out)

class synapse:
    def __init__(self, i, y): #neurone + liste des branchemnts
        self.value = 2 * np.random.random((i,y)) - 1

class network:
    lay = []
    inp = []
    out = []
    def __init__(self):
        self.derivate = deriv_sigmanoid
        self.fun = sigmanoid

    def buildSynapses(self):
        self.syn = []
        # création des synapses
        for i in range(len(self.lay)-1):
            self.syn.append(synapse(self.lay[i],self.lay[i+1]))
    #apprentissage
    def learn(self,cycles=1,dataInp=[],dataOutp=[],propList=[]):
        if dataInp == []:
            dataIn = self.inp
            dataOut = self.out
        else:
            dataIn = dataInp
            dataOut = dataOutp
        for i in range(cycles):
            if dataInp == []:
                l = self.propagate(dataIn)
            else :
                l = propList
            error = []
            delta = []
            error.insert(0,dataOut - l[-1]) # calcul de l'erreur finale
            delta.insert(0,error[0] * self.derivate_fun(l[-1])) # calcule de la correction
            for i in range(len(self.syn)-1): # on applique pour les couches internes
                error.insert(0,delta[0].dot(self.syn[-(1+i)].value.T)) #calcul des erreurs par couche
                delta.insert(0,error[0] * self.derivate_fun(l[-(2+i)])) # calcul des corrections par couche
            for i in range(len(self.syn)):
                self.syn[i].value += l[i].T.dot(delta[i])
        return np.max(error[-1])
    
    def derivate_fun(self,x):
        return self.derivate(x)

    def propagate_fun(self,x):
        return self.fun(x)

    def propagate(self,entry):        
        l = []
        l.append(entry)
        for elem in self.syn:
            l.append(self.propagate_fun(np.dot(l[-1],elem.value))) # calcul par couche de chaque neurone
        return l
    def calcule(self,entry):
        return self.propagate(entry)[-1]
    def affine(self,propList,inp,out):
        return self.learn(inp,out,propList)
