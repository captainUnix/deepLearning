#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
import deepLearning as dl

net = dl.network()
net.lay = [4,4,4,3,1]

net.inp = np.array([[0,0,1,0],
            [1,0,1,0],
            [0,1,1,0],
            [1,1,1,0]])

net.out = np.array([[0],[1],[1],[0]])
net.buildSynapses()

rec = np.array([])
val = np.array([])

"""
for i in range (10000):
    error = net.learn()
    if ((i%1000) == 0):
        rec = np.append(rec,[i])
        val = np.append(val,[error])


plt.plot(rec, 1-val)
plt.show()
"""
error = net.learn(50000)
print("Max error : "+ str(error))
